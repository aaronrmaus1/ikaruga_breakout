using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    public static Music instance;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip mainMusic;
    [SerializeField] AudioClip endMusic;

    void Awake()
    {
        if (instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        audioSource.volume = 0.4f;
        audioSource.clip = mainMusic;
        audioSource.loop = true;
        audioSource.Play();
    }

    private IEnumerator FadeMusic(float time, float startTime)
    {
        while ((Time.time - startTime) < time)
        {
            audioSource.volume = 0.4f * (1 - (Time.time - startTime) / time);
            yield return null;
        }
        audioSource.volume = 0.4f;
        audioSource.clip = endMusic;
        audioSource.loop = false;
        audioSource.Play();
        yield return null;
    }

    public void PlayEndClip()
    {
        StartCoroutine(FadeMusic(2f, Time.time));
    }

 
}
