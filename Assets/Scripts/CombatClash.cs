using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatClash : MonoBehaviour
{
    [SerializeField] private float defaultDuration; // seconds
    [SerializeField] private float defaultExtensionTime; // seconds added per unit joining the clash
    [SerializeField] private float perimeterDrawOuterOffset; // how far past the farthest soldier to draw the perimeter
    [SerializeField] private float resetMultiplierPenalty; // yo dawg, it's a multiplier for the multiplier so you can multiply your multiplication.

    [SerializeField] AudioSource AudioSource;

    private float startTime = 0; // offset from level load
    private float endTime = 0; // offset from level load
    private float clashRadius = 0; // calculated in response to updates
    private float resetMultiplier = 1.0f;

    private Dictionary<TowerScript.Team, List<SoldierScript>> soldiersByTeam;

    // Called after construction. Every clash must have at least 2 soldiers
    public void InitializeClash(SoldierScript s1, SoldierScript s2) {
        soldiersByTeam = new Dictionary<TowerScript.Team, List<SoldierScript>>();
        IncludeSoldier(s1);
        IncludeSoldier(s2);
        AudioSource.Play();
    }

    // Called when a soldier is added to the clash
    public void IncludeSoldier(SoldierScript newSoldier) {
        List<SoldierScript> soldiers;
        if(!soldiersByTeam.TryGetValue(newSoldier.GetTeam(), out soldiers)) {
            soldiers = new List<SoldierScript>();
            soldiersByTeam[newSoldier.GetTeam()] = soldiers;
        }

        if(!soldiers.Contains(newSoldier)) {
            soldiers.Add(newSoldier);
        }

        // Add to timer when soldier is added to existing clash
        if(startTime > 0) {
            // reset timer when a new soldier joins the fight
            ResetTimer();
            RecalculateRadius();
            RecalculateCenter();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        ResetTimer();
        RecalculateRadius();
        RecalculateCenter();
    }

    private void ResetTimer() {
        startTime = Time.timeSinceLevelLoad;
        endTime = startTime + (defaultDuration * resetMultiplier);
        // assumes the penalty is 0 > x < 1
        resetMultiplier *= resetMultiplierPenalty;
    }

    // Update is called once per frame
    void Update()
    {
        DrawPerimeter();
        if (Time.timeSinceLevelLoad > endTime) {
            Resolve();
        }
    }

    // Called when the timer is up and the clash effects should be distributed
    void Resolve() {
        // Note: this assumes only two teams.
        //Debug.Log("Resolving clash between " + soldiersByTeam.Count + " teams.");
        List<SoldierScript> redSoldiers = soldiersByTeam[TowerScript.Team.red];
        List<SoldierScript> blueSoldiers = soldiersByTeam[TowerScript.Team.blue];
        //Debug.Log("Resolving clash between " + redSoldiers.Count + " red soldiers & " + blueSoldiers.Count + " blue soldiers...");

        while(redSoldiers.Count > 0) {
            // peek a red soldier - this one should always have *some* health
            SoldierScript red = redSoldiers[0];
            
            // if there's a blue soldier left, peek it as well
            if(blueSoldiers.Count > 0) {
                SoldierScript blue = blueSoldiers[0];

                // each soldier hits the other for 1 damage at a time
                red.ReduceHP(1);
                blue.ReduceHP(1);

                if(red.GetHP() <= 0) {
                    red.Die(true);
                    redSoldiers.RemoveAt(0);
                }
                if(blue.GetHP() <= 0) {
                    blue.Die(true);
                    blueSoldiers.RemoveAt(0);
                }
            }
            else {
                // otherwise we're done
                break;
            }
        }

        if(redSoldiers.Count > 0 && blueSoldiers.Count > 0) {
            Debug.Log("WHAT THE FUCK? This shouldn't happen.");
        }

        if (redSoldiers.Count > 0) {
            //Debug.Log("Clash resolution: red wins! Remaining soldiers: " + redSoldiers.Count);
            foreach (SoldierScript soldier in redSoldiers)
            {
                soldier.LeaveClash();
            }
        }
        else if(blueSoldiers.Count > 0) {
            //Debug.Log("Clash resolution: blue wins! Remaining soldiers: " + blueSoldiers.Count);
            foreach (SoldierScript soldier in blueSoldiers)
            {
                soldier.LeaveClash();
            }
        }
        else {
            //Debug.Log("Clash resolution: draw! No remaining soldiers on either side.");
        }

        // Finally destroy the clash itself
        Destroy(gameObject);
    }

    private void DrawPerimeter() {
        float radius = Mathf.Lerp(clashRadius + perimeterDrawOuterOffset, clashRadius/2.0f, GetLifeRatio());

        var segments = 16;
        var lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.useWorldSpace = false;
        lineRenderer.startWidth = 0.1f;
        lineRenderer.endWidth = 0.1f;
        lineRenderer.positionCount = segments + 1;

        var pointCount = segments + 1; // add extra point to make startpoint and endpoint the same to close the circle
        var points = new Vector3[pointCount];

        for (int i = 0; i < pointCount; i++) {
            var rad = Mathf.Deg2Rad * (i * 360f / segments);
            points[i] = new Vector3(Mathf.Sin(rad) * radius, 0, Mathf.Cos(rad) * radius);
        }
        lineRenderer.SetPositions(points);

    }

    private float GetLifeRatio() {
        float totalLifetime = endTime - startTime;
        float elapsed = Time.timeSinceLevelLoad - startTime;
        return elapsed / totalLifetime;
    }
    
    private float RecalculateRadius() {
        // determine radius as outer-most soldier, plus a buffer
        float radiusSqr = 0;
        foreach (var team in soldiersByTeam) {
            foreach (var soldier in team.Value) {
                float newRadiusSqr = (soldier.transform.position - transform.position).sqrMagnitude;
                if (newRadiusSqr > radiusSqr) {
                    radiusSqr = newRadiusSqr;
                }
            }
        }

        clashRadius = Mathf.Sqrt(radiusSqr);
        return clashRadius;
    }

    private Vector3 RecalculateCenter() {
        int count = 0;
        Vector3 pos = new Vector3();
        foreach (var team in soldiersByTeam) {
            foreach (var soldier in team.Value) {
                pos += soldier.transform.position;
                count++;
            }
        }
        pos /= count;
        this.transform.position = pos;
        return pos;
    }

    public float GetClashRadius() {
        return clashRadius;
    }

}
