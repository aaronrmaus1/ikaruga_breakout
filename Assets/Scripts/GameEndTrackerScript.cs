using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameEndTrackerScript : MonoBehaviour
{
    public List<GameObject> redTowers;
    public List<GameObject> blueTowers;
    public List<GameObject> redSoldiers;
    public List<GameObject> blueSoldiers;

    [SerializeField] Text gameOverText;
    [SerializeField] Image redWinImage;
    [SerializeField] Image blueWinImage;
    [SerializeField] float gameEndLengthTime;
    private float gameEndTime;

    public static GameEndTrackerScript instance;

    public enum GameState { pre, active, end};
    public GameState CurrentGameState;
    public void AddRedTower(GameObject gameObject)
    {
        redTowers.Add(gameObject);
    }

    public void AddBlueTower(GameObject gameObject)
    {
        blueTowers.Add(gameObject);
    }

    public void AddRedSoldier(GameObject gameObject)
    {
        redSoldiers.Add(gameObject);
    }

    public void AddBlueSoldier(GameObject gameObject)
    {
        blueSoldiers.Add(gameObject);
    }

    public void RemoveRedTower(GameObject gameObject)
    {
        redTowers.Remove(gameObject);
    }

    public void RemoveBlueTower(GameObject gameObject)
    {
        blueTowers.Remove(gameObject);
    }

    public void RemoveRedSoldier(GameObject gameObject)
    {
        redSoldiers.Remove(gameObject);
    }

    public void RemoveBlueSoldier(GameObject gameObject)
    {
        blueSoldiers.Remove(gameObject);
    }

    void Awake()
    {
        if (instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        CurrentGameState = GameState.pre;
        redTowers = new List<GameObject>();
        blueTowers = new List<GameObject>();
        redSoldiers = new List<GameObject>();
        blueSoldiers = new List<GameObject>();
    }


    // Start is called before the first frame update
    void Start()
    {   
        gameOverText.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        switch (CurrentGameState) 
        {
            case GameState.pre:
                break;
            case GameState.active:
                if ((redTowers.Count == 0) | (blueTowers.Count == 0))
                {
                    UIScoreKeeperScript.instance.SetGameOver(true);
                    Music.instance.PlayEndClip();
                    gameOverText.enabled = true;
                    gameOverText.text = "The war is over!\nMany lives were lost,\nbut you made " + UIScoreKeeperScript.instance.playerScore + " gold!";
                    CurrentGameState = GameState.end;
                    gameEndTime = Time.time;

                    if (redTowers.Count == 0)
                    {
                        blueWinImage.enabled = true;
                        
                    }
                    else
                    {
                        redWinImage.enabled = true;
                    }

                    //foreach (GameObject obj in redSoldiers)
                    //{
                    //    //obj.SetActive(false);
                    //    Destroy(obj);
                    //}
                    //foreach (GameObject obj in blueSoldiers)
                    //{
                    //    obj.SetActive(false);
                    //    Destroy(obj);
                    //}                    
                }
                else
                {
                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        foreach (GameObject obj in redSoldiers)
                        {
                            obj.GetComponent<SoldierScript>().ShowLine(true);
                        }
                        foreach (GameObject obj in blueSoldiers)
                        {
                            obj.GetComponent<SoldierScript>().ShowLine(true);
                        }
                    }

                    if (Input.GetKeyUp(KeyCode.Space))
                    {
                        foreach (GameObject obj in redSoldiers)
                        {
                            obj.GetComponent<SoldierScript>().ShowLine(false);
                        }
                        foreach (GameObject obj in blueSoldiers)
                        {
                            obj.GetComponent<SoldierScript>().ShowLine(false);
                        }
                    }
                }
                break;
            case GameState.end:
                if (Time.time - gameEndTime >= gameEndLengthTime)
                {

                    SceneManager.LoadScene("StartMenu", LoadSceneMode.Single);
                }
                break;
        }
        
    }
}
