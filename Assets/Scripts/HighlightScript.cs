using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightScript : MonoBehaviour
{
    [SerializeField] private Renderer myRenderer;
    // Start is called before the first frame update

    public void EnableHighlight(bool enabled)
    {
        myRenderer.enabled = enabled;
    }
    void Start()
    {
        myRenderer.material.color = Color.yellow;
        myRenderer.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
