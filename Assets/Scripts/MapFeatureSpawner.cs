using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapFeatureSpawner : MonoBehaviour
{
    [SerializeField] Vector3 spawnLocation; // location the spawner will use for placement
    [SerializeField] GameObject[] featurePrefabs; // the features the spawner can choose from for placement.

    private Unity.AI.Navigation.NavMeshSurface navMeshSurface;

    // Start is called before the first frame update
    void Start()
    {
        // TODO: Spawn one feature from prefabs
        var selectedPrefab = featurePrefabs[Random.Range(0, featurePrefabs.Length)];
        // We spawn the feature using the prefab's rotation so that each one is oriented correctly. Since we override the spawn position, each prefab's spawn position contains a local offset adjusted to make it fit the map (typically raising up / down for minor adjustment).
        var location = spawnLocation + selectedPrefab.transform.position;

        // TODO: Rotate prefab's rotation by random Y rotation (requires thought because forests shouldn't be rotated)
        GameObject featureGo = Instantiate(selectedPrefab, location, selectedPrefab.transform.rotation, transform);

        // TODO: Recalculate navMeshSurface? Doesn't appear to be necessary.
        //navMeshSurface = GetComponent<Unity.AI.Navigation.NavMeshSurface>();
        //navMeshSurface.BuildNavMesh();
    }
}
