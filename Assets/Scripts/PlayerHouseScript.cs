using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHouseScript : MonoBehaviour
{
    public static Color SelectionColor = Color.yellow;

    // All the states the player house can be in when selecting / commanding units. This can change if it's not the right concept later on.
    protected enum SelectionState
    {
        NoSelection = 0,
        SoldierSelected = 1,
        TowerSelected = 2,
    };

    protected SelectionState selectionState;
    protected GameObject selectedObject;
    protected Color selectedObjectStartColor;

    // Start is called before the first frame update
    void Start()
    {
        selectionState = SelectionState.NoSelection;
        selectedObject = null;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0)) {
            HandleMouseDown(0);
        }
        else if (Input.GetMouseButtonDown(1))
        {
            HandleMouseDown(1);
        }
    }

    void HandleMouseDown(int buttonNum) {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (!Physics.Raycast(ray, out hit)) {
            return; // no hit, no work
        }
        if(hit.collider == null || hit.collider.gameObject == null) {
            return; // no game object, no work
        }
        
        // it's possible what we're commanding is no longer alive, so handle that...
        if(selectedObject == null && selectionState != SelectionState.NoSelection) {
            Debug.Log("Player selection state was bad (null selection but selection state was "+ selectionState + "); Resetting state to NoSelection.");
            ClearSelectionState();
        }

        if (buttonNum == 0)
        {
            switch (selectionState)
            {
                case SelectionState.SoldierSelected:
                    {
                        Debug.Log("Attempting to command soldier with parameter: " + hit.collider.gameObject);
                        if (TryCommandSoldier(hit.collider.gameObject))
                        {
                            ClearSelectionState();
                        }
                        else if (hit.collider.gameObject.GetComponent<SoldierScript>() != null)
                        {
                            Debug.Log("Changing selection to new solider " + hit.collider.gameObject + " for next command.");
                            ClearSelectionState();
                            selectedObject = hit.collider.gameObject;
                            selectionState = SelectionState.SoldierSelected;
                            OnNewSelectedObject(selectedObject);
                        }
                    }
                    break;
                case SelectionState.TowerSelected:
                    {
                        Debug.Log("Attempting to command tower with parameter: " + hit.collider.gameObject);
                        if (TryCommandTower(hit.collider.gameObject))
                        {
                            ClearSelectionState();
                        }
                    }
                    break;
                case SelectionState.NoSelection:
                    {
                        if (hit.collider.gameObject.GetComponent<SoldierScript>() != null)
                        {
                            Debug.Log("Selecting soldier " + hit.collider.gameObject + " for next command.");
                            // soldier found
                            selectedObject = hit.collider.gameObject;
                            selectionState = SelectionState.SoldierSelected;
                            OnNewSelectedObject(selectedObject);

                        }
                        else if (hit.collider.gameObject.GetComponent<TowerScript>() != null)
                        {
                            Debug.Log("Tower not currently selectable for primary command.");
                            //Debug.Log("Selecting tower " + hit.collider.gameObject + " for next command.");
                            //// tower found
                            //selectedObject = hit.collider.gameObject;
                            //selectionState = SelectionState.TowerSelected;
                            //OnNewSelectedObject(selectedObject);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        else if (buttonNum == 1 & hit.collider.gameObject.GetComponent<SoldierScript>() != null)
        {
            if (hit.collider.gameObject.GetComponent<SoldierScript>().Enhance())
            {
                UIScoreKeeperScript.instance.AbilityUsed(1);
            }
        }
    }

    // Returns true if the soldier was able to take action on the designated target object
    bool TryCommandSoldier(GameObject targetObject) {
        SoldierScript selectedSoldier = selectedObject.GetComponent<SoldierScript>(); // this should succeed if we're in a state to call this function
        TowerScript tower = targetObject.GetComponent<TowerScript>();
        if (tower != null) {
            // We've commanded the soldier to go towards a new tower.
            selectedSoldier.SetTarget(tower.transform.position, false);
            UIScoreKeeperScript.instance.AbilityUsed(0);
            SoundFX.instance.PlayRedirectClip();
            return true;
        }

        CombatClash clash = targetObject.GetComponent<CombatClash>();
        if(clash != null) {
            // Change the soldier's current waypoint to the new clash
            selectedSoldier.SetOrReplaceWaypoint(clash.transform.position);
            UIScoreKeeperScript.instance.AbilityUsed(0);
            SoundFX.instance.PlayRedirectClip();
            return true;
        }

        SoldierScript soldier = targetObject.GetComponent<SoldierScript>();
        if(soldier != null) {
            clash = soldier.GetClash();
            if(clash != null) {
                // Change the soldier's current waypoint to the new clash
                selectedSoldier.SetOrReplaceWaypoint(clash.transform.position);
                UIScoreKeeperScript.instance.AbilityUsed(0);
                SoundFX.instance.PlayRedirectClip();
                return true;
            }
        }


        Debug.Log("Soldier did not know what to do with " + gameObject + ", awaiting another command...");
        return false;
    }

    bool TryCommandTower(GameObject targetObject) {
        Debug.Log("TODO: Towers don't support any sort of commands yet. Awaiting a new selection...");
        // TODO: No actions yet for towers
        return true;
    }

    void OnNewSelectedObject(GameObject selected) {
        //Renderer r = selected.GetComponent<Renderer>();
        //selectedObjectStartColor = r.material.color;
        //r.material.color = SelectionColor;
        selected.GetComponent<HighlightScript>().EnableHighlight(true);

        if (selected.GetComponent<SoldierScript>() != null)
        {
            selected.GetComponent<SoldierScript>().SetSelected(true);
        }
    }

    void ClearSelectionState() {
        if(selectedObject != null) {
            //selectedObject.GetComponent<Renderer>().material.color = selectedObjectStartColor;
            selectedObject.GetComponent<HighlightScript>().EnableHighlight(false);
            if (selectedObject.GetComponent<SoldierScript>() != null)
            {
                selectedObject.GetComponent<SoldierScript>().SetSelected(false);
            }
        }
        selectedObject = null;
        selectionState = SelectionState.NoSelection;
    }
}
