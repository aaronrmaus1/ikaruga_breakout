using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierScript : MonoBehaviour
{

    protected Vector3 target;
    protected Vector3 originalTarget;
    protected Vector3 waypoint;
    [SerializeField] float moveSpeed;
    [SerializeField] float slowMoveSpeed;
    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private LineRenderer lineRenderer;
    [SerializeField] private float[] spawnBoundsX;
    [SerializeField] private float[] spawnBoundsY;

    [SerializeField] private int baseHP; // HP of a soldier
    [SerializeField] private int enhancedHP; // HP of a soldier while enhanced

    [SerializeField] private Transform modelTransform;
    private TowerScript.Team myTeam;

    [SerializeField] private GameObject clashPrefab;

    [SerializeField] private GameObject regularSoldierModel;
    [SerializeField] private GameObject mutantSoldierModel;
    [SerializeField] private Animator animator;
    [SerializeField] private RuntimeAnimatorController regularAnimController; 
    [SerializeField] private RuntimeAnimatorController mutantAnimController;
    [SerializeField] private Avatar regularAvatar;
    [SerializeField] private Avatar mutantAvatar;

    private bool isReachedWaypoint;
    private bool isSelected;
    private bool isEnhanced;

    private int currentHP;

    private CombatClash currentClash;

    public int GetHP() {
        return currentHP;
    }

    public void ReduceHP(int damage) {
        currentHP -= damage;
    }

    public bool IsEnhanced() {
        return isEnhanced;
    }

    public void SetTarget(Vector3 newTarget, bool isWaypoint)
    {
        if (isEnhanced)
        {
            return;
        }

        if (isWaypoint)
        {
            target = new Vector3(newTarget.x, 0.5f, newTarget.z);
            originalTarget = target;
            waypoint = new Vector3(Random.Range(spawnBoundsX[0], spawnBoundsX[1]), 0.5f, Random.Range(spawnBoundsY[0], spawnBoundsY[1]));

            float randomRotation = Random.Range(1f, 7f);
            randomRotation = randomRotation * Mathf.Sign(Random.Range(-1f, 1f));
            target = Quaternion.Euler(0, randomRotation, 0) * target;
            target = (target - waypoint).normalized * 100f + waypoint;

            // TODO: Remove target / originalTarget stuff. What's below uses nav agent to calculate a path
            var agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
            if(agent != null) {
                agent.SetDestination(waypoint);
            }

            lineRenderer.positionCount = 3;
            lineRenderer.SetPosition(0, this.transform.position);
            lineRenderer.SetPosition(1, waypoint);
            lineRenderer.SetPosition(2, target);
            isReachedWaypoint = false;
            lineRenderer.gameObject.SetActive(false);
        }
        else
        {
            target = new Vector3(newTarget.x, 0.5f, newTarget.z);
            originalTarget = target;
            lineRenderer.positionCount = 2;
            lineRenderer.SetPosition(0, this.transform.position);
            lineRenderer.SetPosition(1, target);
            isReachedWaypoint = true;
            lineRenderer.gameObject.SetActive(false);
            var agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
            if (agent != null) {
                agent.SetDestination(target);
            }
        }
        
    }

    public void SetOrReplaceWaypoint(Vector3 newWaypoint) {
        waypoint = newWaypoint;

        float randomRotation = Random.Range(1f, 7f);
        randomRotation = randomRotation * Mathf.Sign(Random.Range(-1f, 1f));
        target = Quaternion.Euler(0, randomRotation, 0) * originalTarget;
        target = (target - waypoint).normalized * 100f + waypoint;

        var agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        if (agent != null) {
            agent.SetDestination(waypoint);
        }

        lineRenderer.positionCount = 3;
        lineRenderer.SetPosition(0, this.transform.position);
        lineRenderer.SetPosition(1, waypoint);
        lineRenderer.SetPosition(2, target);
        isReachedWaypoint = false;
    }

    public void SetSelected(bool IsSelected)
    {
        isSelected = IsSelected;
        lineRenderer.gameObject.SetActive(IsSelected);
    }

    public void ShowLine(bool showLine)
    {
        lineRenderer.gameObject.SetActive(showLine | isSelected);
    }

    private void SetMeshColor(Color color)
    {
        meshRenderer.materials[0].color = color;
    }

    private void SetLayer(int layerNum)
    {
        this.gameObject.layer = layerNum;
        for (int i=0; i< this.gameObject.transform.childCount; i++)
        {
            this.gameObject.transform.GetChild(i).gameObject.layer = layerNum;
        }
    }

    public void SetTeam(TowerScript.Team team)
    {
        myTeam = team;
        if (team == 0)
        {
            SetMeshColor(Color.red);
            SetLayer(LayerMask.NameToLayer("Red"));
            lineRenderer.startColor = Color.red;
            lineRenderer.endColor = Color.red;
        }
        else
        {

            SetMeshColor(Color.blue);
            SetLayer(LayerMask.NameToLayer("Blue"));
            lineRenderer.startColor = Color.blue;
            lineRenderer.endColor = Color.blue;
        }
    }

    public TowerScript.Team GetTeam() {
        return myTeam;
    }

    public void SwitchTeams()
    {
        myTeam = 1 - myTeam;
        SetTeam(myTeam);

        if (myTeam == 0)
        {
            GameEndTrackerScript.instance.AddRedSoldier(this.gameObject);
            GameEndTrackerScript.instance.RemoveBlueSoldier(this.gameObject);
        }
        else
        {
            GameEndTrackerScript.instance.RemoveRedSoldier(this.gameObject);
            GameEndTrackerScript.instance.AddBlueSoldier(this.gameObject);
        }
    }

    // This returns the current clash only if it is valid still
    public CombatClash GetClash() {
        if(IsInClash()) {
            return currentClash;
        }
        return null;
    }

    /*private void MoveTowardsTarget()
    {
        if (isReachedWaypoint)
        {
            this.transform.Translate((target - this.transform.position).normalized * moveSpeed * Time.deltaTime);
            lineRenderer.SetPosition(0, this.transform.position);
            modelTransform.localRotation = Quaternion.LookRotation(target - this.transform.position);
        }
        else
        {
            this.transform.Translate((waypoint - this.transform.position).normalized * moveSpeed * Time.deltaTime);
            lineRenderer.SetPosition(0, this.transform.position);
            modelTransform.localRotation = Quaternion.LookRotation(waypoint - this.transform.position);
        }

        if ((waypoint - this.transform.position).magnitude <= 0.1)
        {
            isReachedWaypoint = true;
            lineRenderer.positionCount = 2;
            lineRenderer.SetPosition(0, this.transform.position);
            lineRenderer.SetPosition(1, target);
        }
    }*/

    public void Die(bool isPlayDieSound=false)
    {
        if (this.gameObject == null)
        {
            return;
        }

        if (isPlayDieSound)
        {
            SoundFX.instance.PlayDeathClip();
        }

        if (myTeam == 0)
        {
            GameEndTrackerScript.instance.RemoveRedSoldier(this.gameObject);
        }
        else 
        {
            GameEndTrackerScript.instance.RemoveBlueSoldier(this.gameObject);
        }
        Destroy(this.gameObject);
    }

    public bool Enhance()
    {
        if (!isEnhanced)
        {
            //modelTransform.localScale *= 2;
            regularSoldierModel.SetActive(false);
            mutantSoldierModel.SetActive(true);

            animator.runtimeAnimatorController = mutantAnimController;
            animator.avatar = mutantAvatar;

            currentHP = enhancedHP;

            // On enhance, randomly pick new tower target without waypoint.
            Vector3 target;
            if (myTeam == TowerScript.Team.red)
            {
                target = TowerSpawnerScript.instance.blueTowers[Random.Range(0, TowerSpawnerScript.instance.blueTowers.Count)].transform.position;
            }
            else
            {
                target = TowerSpawnerScript.instance.redTowers[Random.Range(0, TowerSpawnerScript.instance.redTowers.Count)].transform.position;
            }

            SoundFX.instance.PlayEnhanceClip();

            SetTarget(target, false);
            isEnhanced = true;

            return true;
        }
        return false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(IsInClash()) {
            return; // nothing to do when in a clash, we shouldn't really be entering any new triggers
        }

        if (other.tag == "SlowZone") {
            var agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
            agent.speed = slowMoveSpeed;
            return; // end early so that we don't kill ourselves below when the layers don't match
        }

        if (other.gameObject.layer != this.gameObject.layer) {
            // When we enter a slow zone, slow the movement down.

            // if we ran into a soldier of either team, we either need to create a clash or join one
            SoldierScript otherSoldier = other.gameObject.GetComponent<SoldierScript>();
            if(otherSoldier != null) {
                CombatClash clash = otherSoldier.GetClash();
                if(clash != null) {
                    //Debug.Log(this + "is joining " + otherSoldier + "'s clash: " + clash);
                }
                else {
                    //Debug.Log(this + " is creating new clash with " + otherSoldier);
                    Vector3 clashPos = (transform.position + otherSoldier.transform.position) / 2.0f;
                    GameObject clashGo = Instantiate(clashPrefab, clashPos, Quaternion.identity);
                    clash = clashGo.GetComponent<CombatClash>();
                    clash.InitializeClash(this, otherSoldier);
                }
                EnterClash(clash);
            }
            else {
                // TODO: We probably want something more than Die() to happen when a soldier hits something other than another soldier. Or maybe not?
                Die(false);
            }
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.tag == "SlowZone") {
            var agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
            agent.speed = moveSpeed;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        currentHP = isEnhanced ? enhancedHP : baseHP;

        var agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        agent.updateRotation = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(lineRenderer.gameObject.activeSelf) {
            // continuously update the first point, which is the beginning of the line (us)
            lineRenderer.SetPosition(0, this.transform.position);
        }

        if (IsInClash()) {
            
        }
        else {
            // not in a clash, so move
            //MoveTowardsTarget();
            UpdateNavGoal();

            // This only really runs when you tell the soldier to go towards their own tower. Shouldn't be an issue.
            if ((target - this.transform.position).sqrMagnitude <= 0.1f) {
                Debug.Log("Soldier died from reaching destination.");
                Die(false);
            }
        }
        
    }

    private void UpdateNavGoal() {
        var agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        if (agent != null) {
            if (!isReachedWaypoint && HasReachedNavGoal()) {
                agent.SetDestination(originalTarget);

                isReachedWaypoint = true;
                //lineRenderer.positionCount = 2;
                //lineRenderer.SetPosition(0, this.transform.position);
                //lineRenderer.SetPosition(1, originalTarget);
            }
            else
            {
                //lineRenderer.SetPosition(0, this.transform.position);
                
            }
        }
        lineRenderer.positionCount = agent.path.corners.Length;
        lineRenderer.SetPositions(agent.path.corners);

    }

    bool IsInClash() {
        return currentClash != null && currentClash.gameObject != null;
    }

    public void EnterClash(CombatClash combatClash)
    {
        currentClash = combatClash;
        transform.rotation = Quaternion.LookRotation(combatClash.transform.position - this.transform.position);
        this.gameObject.GetComponent<Animator>().Play("Swinging");
        currentClash.IncludeSoldier(this);
        var agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        agent.isStopped = true;
    }

    public void LeaveClash() 
    {
        // currently we can't leave an ongoing clash so this only gets called once the clash is over
        currentClash = null;
        this.gameObject.GetComponent<Animator>().Play("Walking");
        var agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        agent.isStopped = false;
    }

    private bool HasReachedNavGoal() {
        var agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        if (agent != null) {
            return ((agent.pathStatus == UnityEngine.AI.NavMeshPathStatus.PathComplete) && (agent.remainingDistance == 0));
        }
        return true;
    }
}
