using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFX : MonoBehaviour
{
    public static SoundFX instance;
    [SerializeField] AudioSource AudioSource;
    [SerializeField] AudioClip deathClip;
    [SerializeField] AudioClip enhanceClip;
    [SerializeField] AudioClip buildClip;
    [SerializeField] AudioClip redirectClip;


    public void PlayDeathClip()
    {
        AudioSource.PlayOneShot(deathClip);
    }

    public void PlayEnhanceClip()
    {
        AudioSource.PlayOneShot(enhanceClip);
    }

    public void PlayRedirectClip()
    {
        AudioSource.PlayOneShot(redirectClip);
    }

    public void PlayRebuildClip()
    {
        AudioSource.PlayOneShot(buildClip);
    }

    void Awake()
    {
        if (instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
