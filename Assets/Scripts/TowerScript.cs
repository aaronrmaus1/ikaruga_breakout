using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerScript : MonoBehaviour
{
    // incremented whenever a new soldier is spawned in order to assign new names to ease debugging.
    private static int SoldierSpawnCounter = 0; 

    [SerializeField] private GameObject[] towerModels;
    [SerializeField] private GameObject[] brokenTowerModels;
    [SerializeField] private GameObject[] destroyedTowerModels;
    [SerializeField] private GameObject rubbleModel;
    [SerializeField] private GameObject redSoldierPrefab;
    [SerializeField] private GameObject blueSoldierPrefab;
    [SerializeField] private float minSoldierSpawnTime;
    [SerializeField] private float maxSoldierSpawnTime;

    [SerializeField] private float sendDoubleProb;
    [SerializeField] private float sendEnhancedProb;

    [SerializeField] private int startingHP;

    [SerializeField] private AudioClip spawnSoldierAudioClip;
    [SerializeField] private AudioClip towerDamageAudioClip;
    [SerializeField] private AudioClip towerDestroyAudioClip;
    [SerializeField] private AudioSource audioSource;

    private bool isSpawnRush;

    private int currentHP;

    private float lastSoldierSpawnTime;
    private bool isSpawnDouble;

    private float nextSoldierSpawnTime;

    private Team myTeam;
    private bool isGameOver;
    private TowerState towerState;

    private float destroyedStartTime;
    [SerializeField] private float destroyedShowTime;

    public enum Team { none=-1, red=0, blue=1};
    private enum TowerState { active, broken, destroyed, empty}

    //private void SetMeshColor(Color color)
    //{
    //    meshRenderer.materials[0].color = color;
    //}

    private void UpdateModel()
    {
        if (GameEndTrackerScript.instance.CurrentGameState == GameEndTrackerScript.GameState.active)
        {
            towerModels[0].SetActive(myTeam == Team.red & towerState == TowerState.active);
            towerModels[1].SetActive(myTeam == Team.blue & towerState == TowerState.active);

            brokenTowerModels[0].SetActive(myTeam == Team.red & towerState == TowerState.broken);
            brokenTowerModels[1].SetActive(myTeam == Team.blue & towerState == TowerState.broken);

            destroyedTowerModels[0].SetActive(myTeam == Team.red & towerState == TowerState.destroyed);
            destroyedTowerModels[1].SetActive(myTeam == Team.blue & towerState == TowerState.destroyed);

            rubbleModel.SetActive(towerState == TowerState.empty);
        }
    }

    private void SetLayer(int layerNum)
    {
        this.gameObject.layer = layerNum;
        for (int i = 0; i < this.gameObject.transform.childCount; i++)
        {
            this.gameObject.transform.GetChild(i).gameObject.layer = layerNum;
        }
    }

    public void SetTeam(Team team)
    {
        myTeam = team;
        if (team == Team.red)
        {
            Debug.Log("Setting tower team to red.");
            //SetModel(myTeam);
            SetLayer(LayerMask.NameToLayer("Red"));
        }
        else if(team == Team.blue)
        {
            Debug.Log("Setting tower team to blue.");
            //SetModel(myTeam);
            SetLayer(LayerMask.NameToLayer("Blue"));
        }
        else if(team == Team.none) 
        {
            Debug.Log("Setting tower team to none.");
            //SetModel(myTeam);
            SetLayer(LayerMask.NameToLayer("Neutral"));
        }
        TowerSpawnerScript.instance.SetTeamsTower(this.gameObject, myTeam);
    }

    private void SpawnSoldier()
    {
        UIScoreKeeperScript.instance.SoldierSpawned();
        Vector3 position = new Vector3(this.transform.position.x, 0.5f, this.transform.position.z);
        if (myTeam == Team.red & TowerSpawnerScript.instance.blueTowers.Count > 0)
        {
            Vector3 target = TowerSpawnerScript.instance.blueTowers[Random.Range(0, TowerSpawnerScript.instance.blueTowers.Count)].transform.position;
            //Debug.DrawLine(this.transform.position, target, Color.red, 5);
            //float randomRotation = Random.Range(5f, 15f);
            //randomRotation = randomRotation * Mathf.Sign(Random.Range(-1f, 1f));
            //Debug.Log("Random target direction rotation adjustment: " + randomRotation);
            //target = Quaternion.Euler(0, randomRotation, 0) * target;
            //target = (target - this.transform.position).normalized * 100f + this.transform.position;
            //Debug.DrawLine(this.transform.position, target, Color.red, 5);



            GameObject soldier = Instantiate(redSoldierPrefab, position, Quaternion.identity);
            soldier.name = "Red - " + SoldierSpawnCounter++;
            soldier.GetComponent<SoldierScript>().SetTarget(target, true);
            soldier.GetComponent<SoldierScript>().SetTeam(myTeam);
            GameEndTrackerScript.instance.AddRedSoldier(soldier);

            if (Random.Range(0f, 1f) <= sendEnhancedProb)
            {
                soldier.GetComponent<SoldierScript>().Enhance();
            }

            audioSource.clip = spawnSoldierAudioClip;
            audioSource.Play();
            
        }
        else if(myTeam == Team.blue & TowerSpawnerScript.instance.redTowers.Count > 0)
        {
            Vector3 target = TowerSpawnerScript.instance.redTowers[Random.Range(0, TowerSpawnerScript.instance.redTowers.Count)].transform.position;
            //Debug.DrawLine(this.transform.position, target , Color.blue, 5);
            //float randomRotation = Random.Range(4f, 15f);
            //randomRotation = randomRotation * Mathf.Sign(Random.Range(-1f, 1f));
            //Debug.Log("Random target direction rotation adjustment: " + randomRotation);
            //target = Quaternion.Euler(0, randomRotation, 0) * target;
            //target = (target - this.transform.position).normalized * 100f + this.transform.position;
            //Debug.DrawLine(this.transform.position, target, Color.blue, 5);


            GameObject soldier = Instantiate(blueSoldierPrefab, position, Quaternion.identity);
            soldier.name = "Blue - " + SoldierSpawnCounter++;
            soldier.GetComponent<SoldierScript>().SetTarget(target, true);
            soldier.GetComponent<SoldierScript>().SetTeam(myTeam);
            GameEndTrackerScript.instance.AddBlueSoldier(soldier);

            if (Random.Range(0f, 1f) <= sendEnhancedProb)
            {
                soldier.GetComponent<SoldierScript>().Enhance();
            }

            audioSource.clip = spawnSoldierAudioClip;
            audioSource.Play();
        }
        
    }

    private float UniformToNormalRandom(float min, float max)
    {
        float u1 = Random.Range(0.00000001f, 1f);
        float u2 = Random.Range(0f, 1f);
        float z1 = Mathf.Sqrt(-2f * Mathf.Log(u1)) * Mathf.Cos(2f * Mathf.PI * u2) // Standard box-muller uniform -> normal transformation, capped -4 to +4
            / 8f // scale it to -0.5 to +0.5
            * (max - min) // scale it to the inputted range's scale, e.g. from -(max-min)/2 to +(max-min)/2, centered at 0
            + (max - min) / 2f + min; // shift it to the desired range, from min to max

        return Mathf.Clamp(z1, min, max);
    }



    public void SetSpawnRush(bool IsSpawnRush)
    {
        isSpawnRush = IsSpawnRush;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != this.gameObject.layer)
        {
            SoldierScript otherSoldier = other.gameObject.GetComponent<SoldierScript>();
            if (otherSoldier != null)
            {
                if (myTeam == Team.none)
                {
                    // neutral, rebuild on the soldier's team
                    SetTeam(otherSoldier.GetTeam());
                    towerState = TowerState.active;
                    UpdateModel();
                    UIScoreKeeperScript.instance.TowerSpawned();
                    SoundFX.instance.PlayRebuildClip();
                    currentHP = startingHP;
                }
                else
                {
                    // on an opposing team
                    if (otherSoldier.IsEnhanced())
                    {
                        // Enhanced soldiers instance kill towers, but die in the process themselves :c
                        currentHP = 0;
                    }
                    else
                    {
                        currentHP--;
                    }

                    if (currentHP <= 0)
                    {
                        // Dead, turn to neutral
                        Debug.Log("Destroying Tower.");
                        audioSource.clip = towerDestroyAudioClip;
                        audioSource.Play();
                        //SetTeam(Team.none);
                        towerState = TowerState.destroyed;
                        destroyedStartTime = Time.time;
                        UpdateModel();
                    }
                    else
                    {
                        audioSource.clip = towerDamageAudioClip;
                        audioSource.Play();
                        towerState = TowerState.broken;
                        UpdateModel();
                    }
                }
            }
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        lastSoldierSpawnTime = Time.time - 6;
        //nextSoldierSpawnTime = UniformToNormalRandom(minSoldierSpawnTime, maxSoldierSpawnTime);
        nextSoldierSpawnTime = maxSoldierSpawnTime;
        currentHP = startingHP;
        isGameOver = false;
        isSpawnRush = false;
        towerState = TowerState.active;
        UpdateModel();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - lastSoldierSpawnTime >= nextSoldierSpawnTime && !isGameOver && myTeam != Team.none && towerState != TowerState.destroyed)
        {
            lastSoldierSpawnTime = Time.time;
            if ((Random.Range(0f,1f) <= sendDoubleProb & !isSpawnDouble) | isSpawnRush)
            {
                nextSoldierSpawnTime = minSoldierSpawnTime;
                isSpawnDouble = true;
            }
            else if (isSpawnDouble)
            {
                nextSoldierSpawnTime = maxSoldierSpawnTime;
                isSpawnDouble = false;
            }
            else
            {
                nextSoldierSpawnTime = maxSoldierSpawnTime;
            }
            
            //nextSoldierSpawnTime = UniformToNormalRandom(minSoldierSpawnTime, maxSoldierSpawnTime);
            
            //Debug.Log("Next spawn time: " + nextSoldierSpawnTime);
            SpawnSoldier();
        }

        if (towerState == TowerState.destroyed & Time.time - destroyedStartTime >= destroyedShowTime)
        {
            Debug.Log("Replacing tower rubble with empty foundation.");
            towerState = TowerState.empty;
            SetTeam(Team.none);
            UpdateModel();
        }
    }

    //
    //void OnGUI()
    //{
    //    const int HealthBarSegmentWidth = 20;

    //    Vector2 targetPos;
    //    targetPos = Camera.main.WorldToScreenPoint(transform.position);
    //    // center and position below the unit
    //    targetPos.x -= HealthBarSegmentWidth * ((float)currentHP / 2.0f);
    //    targetPos.y -= 50;

    //    for(int i = 0; i < currentHP; i++) {
    //        GUI.Box(new Rect(targetPos.x, Screen.height - targetPos.y, HealthBarSegmentWidth, HealthBarSegmentWidth), healthBarTexture);
    //        targetPos.x += HealthBarSegmentWidth;
    //    }
        
    //}
}
