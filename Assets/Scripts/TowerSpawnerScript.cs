using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerSpawnerScript : MonoBehaviour
{

    [SerializeField] List<Transform> redSpawnTransforms;
    [SerializeField] List<Transform> blueSpawnTransform;

    [SerializeField] GameObject towerPrefab;

    [HideInInspector]public List<GameObject> redTowers;
    [HideInInspector] public List<GameObject> blueTowers;

    public static TowerSpawnerScript instance;

    [SerializeField] private float spawnRushBufferTime;
    [SerializeField] private float spawnRushLengthTime;
    private float lastSpawnRush;
    private bool isSpawnRush;

    public void SpawnInitialTowers()
    {
        foreach(Transform transform in redSpawnTransforms)
        {
            GameObject redTower = Instantiate(towerPrefab, transform.position, Quaternion.identity);
            redTowers.Add(redTower);
            //GameEndTrackerScript.instance.AddRedTower(redTower);
            redTower.GetComponent<TowerScript>().SetTeam(TowerScript.Team.red);
            //UIScoreKeeperScript.instance.TowerSpawned();
        }

        foreach (Transform transform in blueSpawnTransform)
        {
            GameObject blueTower = Instantiate(towerPrefab, transform.position, Quaternion.identity);
            blueTowers.Add(blueTower);
            //GameEndTrackerScript.instance.AddBlueTower(blueTower);
            blueTower.GetComponent<TowerScript>().SetTeam(TowerScript.Team.blue);
            //UIScoreKeeperScript.instance.TowerSpawned();
        }

        
    }

    public void RemoveTower(GameObject towerObj)
    {
        if (towerObj.layer == LayerMask.NameToLayer("Red"))
        {
            redTowers.Remove(towerObj);
            GameEndTrackerScript.instance.RemoveRedTower(towerObj);
        }
        else
        {
            blueTowers.Remove(towerObj);
            GameEndTrackerScript.instance.RemoveRedTower(towerObj);
        }
    }

    public void SetTeamsTower(GameObject towerObj, TowerScript.Team teamNum)
    {
        if (teamNum == TowerScript.Team.red)
        {
            redTowers.Add(towerObj);
            blueTowers.Remove(towerObj);

            GameEndTrackerScript.instance.AddRedTower(towerObj);
            GameEndTrackerScript.instance.RemoveBlueTower(towerObj);
        }
        else if(teamNum == TowerScript.Team.blue)
        {
            redTowers.Remove(towerObj);
            blueTowers.Add(towerObj);

            GameEndTrackerScript.instance.AddBlueTower(towerObj);
            GameEndTrackerScript.instance.RemoveRedTower(towerObj);
        }
        else {
            redTowers.Remove(towerObj);
            blueTowers.Remove(towerObj);

            GameEndTrackerScript.instance.RemoveRedTower(towerObj);
            GameEndTrackerScript.instance.RemoveBlueTower(towerObj);
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        redTowers = new List<GameObject>();
        blueTowers = new List<GameObject>();
        SpawnInitialTowers();
        GameEndTrackerScript.instance.CurrentGameState = GameEndTrackerScript.GameState.active;
        lastSpawnRush = Time.time;
        isSpawnRush = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isSpawnRush & Time.time - lastSpawnRush >= spawnRushBufferTime)
        {
            isSpawnRush = true;
            lastSpawnRush = Time.time;
            if (Random.Range(0f,1f) <= 0.5f)
            {
                Debug.Log("Starting spawn rush for red.");
                foreach (GameObject redTower in redTowers)
                {
                    redTower.GetComponent<TowerScript>().SetSpawnRush(true);
                    UIPopups.instance.StartRedRush();
                }
                foreach (GameObject redTower in blueTowers)
                {
                    redTower.GetComponent<TowerScript>().SetSpawnRush(false);
                }
            }
            else
            {
                Debug.Log("Starting spawn rush for blue.");
                foreach (GameObject redTower in redTowers)
                {
                    redTower.GetComponent<TowerScript>().SetSpawnRush(false);
                }
                foreach (GameObject blueTower in blueTowers)
                {
                    blueTower.GetComponent<TowerScript>().SetSpawnRush(true);
                    UIPopups.instance.StartBlueRush();
                }
            }
        }
        
        if (isSpawnRush & Time.time - lastSpawnRush > spawnRushLengthTime)
        {
            Debug.Log("Stopping spawn rush.");
            isSpawnRush = false;
            foreach (GameObject redTower in redTowers)
            {
                redTower.GetComponent<TowerScript>().SetSpawnRush(false);
            }
            foreach (GameObject redTower in blueTowers)
            {
                redTower.GetComponent<TowerScript>().SetSpawnRush(false);
            }
        }


    }
}
