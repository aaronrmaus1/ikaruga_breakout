using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScoreKeeperScript : MonoBehaviour
{

    [SerializeField] private Text scoreText;
    public int playerScore;
    [SerializeField] private int goldPerSolidier;
    [SerializeField] private int goldPerTower;
    [SerializeField] private int[] abilityCosts;

    public static UIScoreKeeperScript instance;

    private bool isGameOver;

    public void SetGameOver(bool IsGameOver)
    {
        isGameOver = IsGameOver;
    }
    public void SoldierSpawned()
    {
        playerScore += goldPerSolidier;
        SetScore(playerScore);
    }

    public void TowerSpawned()
    {
        playerScore += goldPerTower;
        SetScore(playerScore);
    }

    public void SetScore(int score)
    {
        if (!isGameOver)
        {
            scoreText.text = "Gold: " + score;
        }
        
    }

    public void AbilityUsed(int abilityNum)
    {
        playerScore -= abilityCosts[abilityNum];
        SetScore(playerScore);
    }

    void Awake()
    {
        if (instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        scoreText.text = "Gold: 0";
        playerScore = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        isGameOver = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
