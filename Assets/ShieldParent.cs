using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldParent : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.LookAt(Camera.main.transform);
        this.transform.localEulerAngles = new Vector3(0, this.transform.localEulerAngles.y, 0);
    }
}
