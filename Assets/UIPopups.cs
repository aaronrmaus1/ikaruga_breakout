using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPopups : MonoBehaviour
{
    public static UIPopups instance;
    [SerializeField] private Image RedRushImage;
    [SerializeField] private Image BlueRushImage;

    private IEnumerator DoFlashRed(float time, float startTime)
    {
        while ((Time.time - startTime) < time)
        {
           RedRushImage.enabled = Mathf.FloorToInt((Time.time - startTime) * 4) % 2 == 0;
           yield return null;
        }
        RedRushImage.enabled = false;
        yield return null;
    }

    private IEnumerator DoFlashBlue(float time, float startTime)
    {
        while ((Time.time - startTime) < time)
        {
            BlueRushImage.enabled = Mathf.FloorToInt((Time.time - startTime) * 3) % 2 == 0;
            yield return null;
        }
        BlueRushImage.enabled = false;
        yield return null;
    }

    public void StartRedRush()
    {
        StartCoroutine(DoFlashRed(2.5f, Time.time));
    }

    public void StartBlueRush()
    {
        StartCoroutine(DoFlashBlue(2.5f, Time.time));
    }

    void Awake()
    {
        if (instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        RedRushImage.enabled = false;
        BlueRushImage.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
